import 'dart:convert';

import 'package:api/insert_date.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class ApiDemo extends StatefulWidget {
  const ApiDemo({Key? key}) : super(key: key);

  @override
  State<ApiDemo> createState() => _ApiDemoState();
}

class _ApiDemoState extends State<ApiDemo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(actions: [
        InkWell(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => InsertData(null),
                ),
              ).then((value) => (value) {
                    if (value == true) {
                      setState(() {});
                    }
                  });
            },
            child: Icon(Icons.add)),
      ]),
      body: FutureBuilder<http.Response>(
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            var apiDate = jsonDecode(snapshot.data!.body);
            return ListView.builder(
              shrinkWrap: true,
              scrollDirection: Axis.vertical,
              itemCount: apiDate.length,
              itemBuilder: (context, index) {
                return InkWell(
                  onTap: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => InsertData(apiDate[index]),
                      )),
                  child: Card(
                      child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        apiDate[index]["laptopName"],
                        style: TextStyle(fontSize: 20),
                      ),
                      Icon(
                        Icons.chevron_right,
                        color: Colors.grey,
                        size: 24,
                      )
                    ],
                  )),
                );
              },
            );
          } else {
            return CircularProgressIndicator();
          }
        },
        future: getAll(),
      ),
    );
  }

  Future<http.Response> getAll() async {
    var res = await http.get(
        Uri.parse('https://63202760e3bdd81d8ef39441.mockapi.io/Books_Api'));
    return res;
  }
}
