import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ApiData extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          var data = json.decode(snapshot.data.toString());
          return ListView.builder(
            shrinkWrap: true,
            itemCount: 3,
            scrollDirection: Axis.vertical,
            itemBuilder: (context, index) {
              return Card(
                child: Text(data["items"][index]["name"].toString()),
              );
            },
          );
        } else {
          return CircularProgressIndicator();
        }
      },
      future: rootBundle.loadString('assets/demo.json'),
    );
  }
}
