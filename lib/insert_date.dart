import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class InsertData extends StatefulWidget {
  Map? map;

  InsertData(this.map);

  @override
  State<InsertData> createState() => _InsertDataState();
}

class _InsertDataState extends State<InsertData> {
  @override
  var formkey = GlobalKey<FormState>();

  var numbercontroller = TextEditingController();

  var namecontroller = TextEditingController();

  Future<http.Response> insertUser() async {
    Map map = {};
    map['laptopName'] = namecontroller.text;
    map['laptopProcessor'] = numbercontroller.text;
    var res = await http.post(
        Uri.parse('https://63202760e3bdd81d8ef39441.mockapi.io/Books_Api'),
        body: map);
    return res;
  }

  Future<http.Response> updateUser(id) async {
    Map map = {};
    map['laptopName'] = namecontroller.text;
    map['laptopProcessor'] = numbercontroller.text;
    var res = await http.put(
        Uri.parse('https://63202760e3bdd81d8ef39441.mockapi.io/Books_Api/$id'),
        body: map);
    return res;
  }

  @override
  void initState() {
    namecontroller.text = widget.map == null ? "" : widget.map!['laptopName'];
    numbercontroller.text =
        widget.map == null ? "" : widget.map!['laptopProcessor'].toString();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: formkey,
        child: Column(
          children: [
            TextFormField(
              controller: namecontroller,
              decoration: InputDecoration(hintText: 'enter name'),
              validator: (value) {
                if (value != null && value.isEmpty) {
                  return "Enter Valid Name";
                }
              },
            ),
            TextFormField(
              controller: numbercontroller,
              decoration: InputDecoration(hintText: 'enter number'),
              validator: (value) {
                if (value != null && value.isEmpty) {
                  return "Enter Valid Name";
                }
              },
            ),
            ElevatedButton(
                onPressed: () {
                  if (formkey.currentState!.validate()) {
                    if (widget.map == null) {
                      insertUser().then((value) => Navigator.pop(context));
                    } else {
                      updateUser(widget.map!["id"]).then((value) {
                        print(value.isRedirect);
                          Navigator.pop(context);
                      });
                    }
                  }
                },
                child: Text('submit'))
          ],
        ),
      ),
    );
  }
}
